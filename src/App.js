import {useState} from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { UserProvider } from "./UserContext";

import AppNavbar from "./components/AppNavbar";

import Courses from "./pages/Courses";
import Error from "./pages/Error";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Register from "./pages/Register";

import {Container} from "react-bootstrap";
import './App.css';

function App() {

  //Global state
  //To store the user information and will be used for validation if a user is already logged in on the app or not.
  const [user, setUser] = useState({
            //null
    email: localStorage.getItem("email")
  })

  //we can check the changes in our User state.
  console.log(user);

  //Function for clearing localStorage on logout
  const unsetUser = () =>{
    localStorage.clear();
  }

  return (

    // In Reactjs, we normally render our components in an entry point or in a mother component called App. This is so we can group our components under a single entry point/main component

    // All other components/pages will be contained in our main component: <App />

    // ReactJS does not like rendering two adjacent elements. instead the adjacent element must be wrapped by a parent element/react fragment

    // ReactJS is a single page application (SPA)
      // Whenever a link is clicked, it functions as if the page is being reloaded but what it actually does is it goes through the process of rendering, mounting, rerendering and unmounting components

    //Wrap all components with our UserProvider component to able our components to access data from our UserContext.

    // We store information in the context by providing the information using the corresponding "Provider" component and passing the information via the "value" prop/attribute.
        //We store a user state and unsetUSer function in our "UserProvider". This information will be pass through all the components and to validate if their is already a logged in user.
    <UserProvider value={{user, setUser, unsetUser}}>
        {/*Router component is used to wrapped around all components which will have access to our routing system.*/}
        <Router>
          <AppNavbar />
          <Container fluid>
              {/*Routes holds all our Route components.*/}
              <Routes>
              {/*
                  - Route assigns an endpoint and displays the appropriate page component for that endpoint.
                  - "path" attribute assigns the endpoint.
                  - "element" attribute assigns page component to be displayed at the endpoint.
              */}
                  <Route exact path ="/" element={<Home />} />
                  <Route exact path ="/courses" element={<Courses />} />
                  <Route exact path ="/register" element={<Register />} />
                  <Route exact path ="/login" element={<Login />} />
                  <Route exact path ="/logout" element={<Logout />} />
                  <Route exact path ="*" element={<Error />} />
              </Routes>
          </Container>
        </Router>
    </UserProvider>
  );
}

/*
  Mini Activity

  1. Create a CourseCard component showing a particular course with the name, description and price inside a React-Bootstrap Card:
    - The course name should be in the card title.
    - The description and price should be in the card subtitle.
    - The value for description and price should be in the card text.
    - Add a button for Enroll.
  2. Render the CourseCard component in the Home page.
  3. Take a screenshot of your browser and send it in the batch hangouts

*/

export default App;
