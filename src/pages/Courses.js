import coursesData from "../data/coursesData";

import CourseCard from "../components/CourseCard";

export default function Courses(){

	//Check if we can access the mock database.

	// console.log(coursesData);
	// console.log(coursesData[0]);

	// Props
		// is a shorthand for property since components are considered as object in ReactJS.
		// Props is a way to pass data from the parent to child component.
		// it is synonymous to the function parameter.

		//map method
		const courses = coursesData.map(course => {
			return(
				<CourseCard key={course.id} courseProp={course} />
			)
		})

	return(
		<>
			<h1>Courses</h1>
			{courses}
		</>
	)
}