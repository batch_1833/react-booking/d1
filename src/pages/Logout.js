import {useContext, useEffect} from "react";
import UserContext from "../UserContext";
import {Navigate} from "react-router-dom";

export default function Logout(){
	// localStorage.clear();

	// Consume the UserContext object and destructure it to access the 
	const {setUser, unsetUser} = useContext(UserContext);

	//Clear the localStorage of the user's information
	unsetUser();
	// console.log(user);

	useEffect(() =>{
		//Set the user state back to it's original value.
		setUser({email: null})
	})

	return(
		//Redirect back to the login
		<Navigate to="/login" />
	)
}