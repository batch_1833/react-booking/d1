import {useState, useEffect, useContext} from "react";
import UserContext from "../UserContext";
import {Navigate} from "react-router-dom";
import {Button, Form} from "react-bootstrap";

export default function Register(){

	const {user} = useContext(UserContext);

	//State hooks to store the values of the input fields
	const [email, setEmail] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");

	//Check if the values are successfully binded/passed.
	console.log(email);
	console.log(password1);
	console.log(password2);

	// Function to simulate user registration
	function registerUser(e){
		//prevents the page redirection via form submit
		e.preventDefault();

		//Clear input fields
		setEmail("");
		setPassword1("");
		setPassword2("");

		alert("Thank you for registering!");
		// email = "";
		// password1 = "";
		// password2 = "";

	}

	//State to determine whether submit button is enabled or not.
	const [isActive, setIsActive] = useState(false);

	// To enable the submit button:
		// No empty input fields.
		// password and verify password should be the same.

	useEffect(()=>{
		if((email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	},[email, password1, password2])

	/*
		Two Way Binding
			- is a way to assure that we can save the input into our states as we type into the input element.

			Syntax:
				<Form.Control type="inputType" value={inputState} onChange={e => {setInputState(e.target.value)}}

					Note:
						- We set the value of the input type to the initialState. We cannot type into our inputs anymore because there is now a value bound to it.
						- The "onChange" event is added to be able to updated the state bound in the input.

				e = event, contains all the details about the event.

				e.target = target is the element WHERE the event happened.

				e.target.value = the current value of the element WHERE the event happened.
	*/

	return(
		(user.email !== null)
		?
			<Navigate to="/courses"/>
		:
		<>
			<h1 className="my-5 text-center">Register</h1>		
			<Form onSubmit = {(e) => registerUser(e)}>
		      <Form.Group className="mb-3" controlId="userEmail">
		        <Form.Label>Email address</Form.Label>
		        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
		        <Form.Text className="text-muted">
		          We'll never share your email with anyone else.
		        </Form.Text>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="password1">
		        <Form.Label>Password</Form.Label>
		        <Form.Control type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)}/>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="password2">
		        <Form.Label>Verify Password</Form.Label>
		        <Form.Control type="password" placeholder="Verify Password" value={password2} onChange={e => setPassword2(e.target.value)}/>
		      </Form.Group>
		      
		      {
		      	isActive
		      	?
		      		<Button variant="primary" type="submit" id="submitBtn">
		      		  Submit
		      		</Button>
		      	:
		      		<Button variant="primary" type="submit" id="submitBtn" disabled>
		      		  Submit
		      		</Button>
		      }
		    </Form>
		</>
	)
}